package app

import (
	"encoding/json"
	"log"
	"sync"
)

type gameInfo struct {
	Id       uint64 `json:"id"`
	Type     string `json:"type"`
	Name     string `json:"name"`
	Endpoint string `json:"endPoint"`
}

type gameApplication interface {
	GetId() uint64
}

// Games pool
func makeGamePool() (p gamePool) {
	p = gamePool{
		Mutex: &sync.Mutex{},
	}
	p.reload()
	return
}

type gamePool struct {
	list map[uint64]gameApplication
	json JsonCache
	*sync.Mutex
}

func (self *gamePool) GetCache() JsonCache {
	self.Lock()
	defer self.Unlock()
	return self.json
}

func (self *gamePool) getById(id uint64) (g gameApplication, ok bool) {
	self.Lock()
	g, ok = self.list[id]
	self.Unlock()
	return
}

func (self *gamePool) reload() {
	self.Lock()
	// Initializing
	self.list = make(map[uint64]gameApplication, 1)
	self.list[1] = newBlackJackApp()

	// Cache for json array
	var err error
	if self.json, err = json.Marshal([]gameApplication{self.list[1]}); err != nil {
		log.Println(err)
	}
	self.Unlock()
}
