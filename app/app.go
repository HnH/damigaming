package app

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/liflandgaming/platform-sdk/lib"
	"github.com/gin-gonic/gin"
)

const (
	OPERATOR = "damigaming"
	PLATFORM = "http://platform-dev.do.optibet.ee:80"
	PORT     = ":8080"
)

type application struct {
	router *gin.Engine
	games  gamePool

	playerApi *swagger.PlayerApi

	dir string
}

var App application

func init() {
	log.SetFlags(log.LstdFlags)

	App = application{
		router: gin.New(),
		games:  makeGamePool(),

		playerApi: swagger.NewPlayerApiWithBasePath(PLATFORM),
	}
}

func Listen() {
	var err error
	if App.dir, err = os.Getwd(); err != nil {
		log.Fatal(err)
	}

	App.router.LoadHTMLGlob(App.dir + "/templates/**/*")

	gr := App.router.Group("/", Middleware())
	{
		gr.GET("/games", handleGetGames)
		gr.GET("/games/:id", handleGetGameById)
		gr.POST("/bj/action", handleBlackJackAction)
	}

	App.router.GET("/bj", handleBlackJackRender)

	go App.router.Run(PORT)

	// Listening to SIGTERM, SIGQUIT and SIGINT
	var (
		chanSignal = make(chan os.Signal, 1)
		s          os.Signal
	)

	signal.Notify(chanSignal, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGKILL)
	s = <-chanSignal
	log.Printf("[SIG] %+v", s)

	os.Exit(1)
}
