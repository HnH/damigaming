package app

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

/**
 * HTTP GET /games
 */
func handleGetGames(c *gin.Context) {
	var rsp = c.MustGet(KEY_RESPONSE).(response)
	rsp["list"] = App.games.GetCache()
}

/**
 * HTTP GET /games/:id
 */
func handleGetGameById(c *gin.Context) {
	var (
		rsp = c.MustGet(KEY_RESPONSE).(response)
		id  uint64
		err error
	)

	if id, err = strconv.ParseUint(c.Param("id"), 10, 0); err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	var ok bool
	if rsp["game"], ok = App.games.getById(id); !ok {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
}
