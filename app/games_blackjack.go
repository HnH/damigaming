package app

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/liflandgaming/platform-sdk"
	"github.com/gin-gonic/gin"
)

const (
	ACTION_BALANCE = "balance"
	ACTION_WIN     = "win"
	ACTION_LOOSE   = "loose"

	CURRENCY_EUR = "EUR"
)

func newBlackJackApp() *BlackJack {
	return &BlackJack{
		&gameInfo{
			Id:       1,
			Type:     "bj",
			Name:     "BlackJack",
			Endpoint: "/bj",
		},
	}
}

type BlackJack struct {
	*gameInfo
}

func (self *BlackJack) GetId() uint64 {
	return self.gameInfo.Id
}

/**
 * HTTP GET /bj
 */
func handleBlackJackRender(c *gin.Context) {
	c.HTML(http.StatusOK, "bj/app.tmpl", gin.H{})
}

/**
 * HTTP POST /bj/action
 */
func handleBlackJackAction(c *gin.Context) {
	c.Request.ParseForm()

	var (
		rsp      = c.MustGet(KEY_RESPONSE).(response)
		playerId = c.Request.Form.Get("player_id")
		action   = c.Request.Form.Get("action")
	)

	if len(action) == 0 || len(playerId) == 0 {
		rsp["error"] = true
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	switch action {
	case ACTION_BALANCE:
		// we can use platform_sdk.GetPlayerCurrency() to get player's currency
		// but for simplicity we assume that everyone has EUR
		// currency, err := platform_sdk.GetPlayerCurrency(App.playerApi, playerId)
		if balance, err := platform_sdk.GetPlayerBalance(App.playerApi, playerId, CURRENCY_EUR); err == nil {
			rsp["balance"] = balance
		} else {
			log.Println(err)
			rsp["error"] = true
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

	case ACTION_WIN:
		var param = c.Request.Form.Get("param")
		if amount, err := strconv.ParseFloat(param, 64); len(param) == 0 || err == nil {
			_, balance, err := platform_sdk.PlayerReceiveFunds(App.playerApi, playerId, OPERATOR, time.Now().String(), CURRENCY_EUR, amount)
			if err == nil {
				rsp["balance"] = balance
			} else {
				rsp["error"] = true
				rsp["errmsg"] = err
			}
		} else {
			rsp["error"] = true
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

	case ACTION_LOOSE:
		var param = c.Request.Form.Get("param")
		if amount, err := strconv.ParseFloat(param, 64); len(param) == 0 || err == nil {
			_, balance, err := platform_sdk.PlayerTakeFunds(App.playerApi, playerId, OPERATOR, time.Now().String(), CURRENCY_EUR, amount)
			if err == nil {
				rsp["balance"] = balance
			} else {
				rsp["error"] = true
				rsp["errmsg"] = err
			}
		} else {
			rsp["error"] = true
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

	default:
		rsp["error"] = true
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	rsp["player_id"] = playerId
	rsp["action"] = action
}
