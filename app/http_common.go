package app

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	KEY_RESPONSE = "rsp"
)

type response map[string]interface{}

// Представление массива в виде JSON строки
func (self response) String() string {
	if b, err := json.Marshal(self); err != nil {
		return "[]"
	} else {
		return string(b)
	}
}

// Basic json api middleware
func Middleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, HEAD, OPTIONS")
		c.Writer.Header().Set(
			"Access-Control-Allow-Headers",
			"Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization",
		)

		c.Set(KEY_RESPONSE, response{"error": false})
		c.Next()

		if c.Writer.Status() == http.StatusOK {
			if len(c.Writer.Header().Get("Content-Type")) == 0 {
				c.Writer.Header().Set("Content-Type", "application/json; encoding=utf-8")
			}

			fmt.Fprint(c.Writer, c.MustGet(KEY_RESPONSE))
		}
	}
}
